# Final Report

Your final report should be viewable in GitLab and editable/resuable by others.
The best text file format for this is markdown.

Here are the elements of a project report.

## Summary (Introduction)

the summary of yoru project should include a user story where you describe a successful use of your application by a user.

For example, Camille and Bhanu's summarizers might go something like this:

> "A student runs a python function on a file path. The function converts the PDF into text and returns 3 sentences that summarize the entire document."

For a chatbot this is called the "happy path" when it describes several statements by the chatbot and the user:

> "A student runs `qary -s summarize` and pastes the URL of a web page into the command line: `YOU: summarize http://en.wikipedia.org/wiki/natural_language`. Qary responds with a list of topics found in the document, such as `BOT: 1) science, 2) technology, 3) NLP, 4) ethics`. User selects a topic from the list with `YOU: nlp`. 

Think about other users and stakeholders that might have an interest in helping you design your final product. In the case of a teaching product, think about what the school administrators, parents, and teachers would think of your product. And is it helping students learn? Or is it a crutch that prevents them from learning the material that the summarizer provides them with.

So you might describe the stakeholders of a summarizing chatbot with something like this.

> "Businesses want to help their employees find and understand the documentation they need. Customers want to find the right section of the documentation to read to help them with their problem.Think about the different people that might have an interest in your project and might want to use it. Managers, users, developers, etc

## ETL: Where did you get the data
## EDA: Visualizations and what they mean to you
## Hyperparameter table (if your project involved machine learning)
## Results: MVP Minimum Viable Product

What were you able to deliver to your "users".
 You can say you succeeded if you are able to deliver this minimumally useful software package, even if you are the only one who can use it.
4. Deliverable: screenshots, documentation, and or a video of your deliverable product and how well it works (accuracy of your model, what users can do with it)

 
