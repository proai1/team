# UX Design

We are in the midst of an IDP (Integrated Design Process) for new kind of messaging and social network app for education.
I'm struck by how our thinking and process could be more *integrated*.
I'm remembering wildly successful and elegant user interfaces: [Zoom](), [Slack](https://en.wikipedia.org/wiki/Slack_Technologies#Slack_and_further_funding), WhatsApp, WhatsApp, Twitter, Facebook, Gmail, and Google Search (in reverse chronological order, like the Twitter and Facebook feeds).
I want our app to be so intuitive and useful that it will grow organically, exponentially, just as these apps did.
I want to find out what it was like on those developer teams in the early days.
Were these internal tools developed by developers for developers? 
Were UX designers doing user interviews or just relying on Beta tester feedback?

## Business Case Studies

Some recent successful startups to spark your memory (most recent first):

- Aug 2013 Release: [Slack](https://en.wikipedia.org/wiki/Slack_(software)#History)
- Sep 2012 Beta: [Zoom](https://en.wikipedia.org/wiki/Zoom_(software)#History)
- Aug 2009 Beta: [WhatsApp](https://en.wikipedia.org/wiki/WhatsApp#History)
- Twitter
- Facebook
- Gmail
- Google Search




From what I remember, these products often came out of an unconventional hacky design process.
Slack was developed internally for the developers of Glitch an MMORPG.
Slack was envisioned as a **S**earchable **L**og of **A**ll **C**onversation and **K**nowledge."
Slack was probably used to streamline the business processes for building Glitch, including IDP!
The Glitch game eventually failed.
I have to assume that they tried to save it with several rounds of IDP.
Perhaps Slack wasn't subjected to that death by a thousand features (and feature cuts).

I'm also thinking of how as those companies grew, that growth was often not good for the UX. 
I'm not talking about the growing pains of scalability or technical debt. 
They all executed well on scaling out to millions of users quickly.
But I think that the demands of investors and customers and even new employees, start to take their toll on the UX.
Those demands and ideas create a sort of random walk in the adding and removal of features.
It's mostly adding.

Slack is much different today than it was when I first installed it in 2013.
Same for Zoom. 
I'm an early adopter so I'll share what I remember of those experiences. 
If you too have influencers in your network that clue you in to up-and-coming apps, and you install new apps early, your comments would be invaluable.
I could add to this dataset.
Early adoption user experience stories (interviews) are incredibly difficult to find.
Once an app explodes in popularity, that flood of feedback crowds out the seminal stories that made the product great in the first place.
I want to collect a few more of those formative stories from developers, designers, and users to see if my hunches are correct.

So I got to see the products early.
And those little tweaks, those incrementtal feature additions, designed by committees of users and developers and designer, they add up. 
And they add up to somthing that is less and less usable for me.
And when that happens, I and other users start looking for alternatives.

So I'm also thinking about alternatives the the conventional IDP process.
This article will show you what I've discovered.
I took a look at those inspiring success stories of UX design.
I'm a Data Scientist after all. 
Data is my professor, my teacher, my source of ideas.
I want the data to tell me how to optimize my process.
So in this article I've compiled a very small sample of user interfaces and whatever information I can glean about the early days of those startups.
And the dataset wouldn't be balanced if I didn't include some clear failures.

And I may try to "lag" the dataset in time.
I want to make sure that the things I'm doing now are predictive of user satistfaction next year and maybe further into the future.
I'll try to use objective stories from news organizations, trade journals, academic journals and 
I'm going going to rely on any one person's wisdom --that what they teach you in school is mostly wrong.
Those awe-inspiring stories of successful UX 
I
But I want it to fullfill the promise of an IDP, truly sustai

## Feature Case Studies

I also want 
