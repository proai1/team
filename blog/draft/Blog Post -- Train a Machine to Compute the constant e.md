# Learn *e*

The mathematical constant `e` is an amazing number. There is a whole [series of wikipedia articles] about it. It allows you to similate an infinite number of time steps and compute exactly what value a system will converge too. Bankers use it to calculate *continuously* [compounding interest](https://en.wikipedia.org/wiki/Compound_interest "Compound interest"). You don't have to recompute the interest and add it to the principle every second or nanosecond. All you do is multiple the principle by e!

The Taylor Series for computing `e` is something like this:

e = sum([n/factorial(n) for n in range(inf)])

The "official" Taylor Series computes `e**x` so the equation for that is:

e = sum([n/factorial(n) for n in range(inf)])

That factorial calculation blows up quickly, so make sure your factorial function is "memoized" so it doesn't take forever.
You probably also want to make it recursive just to simplify the code.
A recursive function is a function that calls itself.
It makes your code harder to read, but computer scientists like it becuase it often leaves less room for error.

```python
def factorial(x):
    """ Compute x! = x * (x - 1) * (x - 2) ... 1  #  x! = 1 for x == 1 or x == 0

    >>> factorial(0)
    1
    >>> factorial(1)
    1
    >>> factorial(3)
    6
    """
    if x in {1, 0}:
        return 1
    else:
        return x * factorial(x - 1)
```

To memoize that, we can just keep track of all the previous factorials we've computed in a dictionary.
```python
def factorial(x, memo={}):
    """ Compute x! = x * (x - 1) * (x - 2) ... 1  #  x! = 1 for x == 1 or x == 0

    >>> factorial(0)
    1
    >>> factorial(1)
    1
    >>> factorial(3)
    6
    """
    if x in {1, 0}:
        return 1
    elif x in memo:
        return memo[x]
    retval = x * factorial(x - 1, memo)
    memo[x] = retval
    return retval
```

So your e function


```ipython
In [1]: (11/10)**10
Out[1]: 2.5937424601000023

In [2]: (10/9)**10
Out[2]: 2.8679719907924426

In [3]: (10/9)**9
Out[3]: 2.5811747917131984

In [4]: (4/3)**3
Out[4]: 2.37037037037037

In [5]: (5/4)**4
Out[5]: 2.44140625

In [6]: (101/100)**100
Out[6]: 2.7048138294215285

In [7]: (1001/1000)**1000
Out[7]: 2.7169239322355936
```
## References

### Wikipedia Article Series on *e*

[A series of articles](https://en.wikipedia.org/wiki/Category:E_(mathematical_constant) "Category:E (mathematical constant)") on the mathematical constant e[![Euler's formula.svg](https://en.wikipedia.org//upload.wikimedia.org/wikipedia/commons/thumb/7/71/Euler%27s_formula.svg/180px-Euler%27s_formula.svg.png)](https://en.wikipedia.org/wiki/File:Euler%27s_formula.svg)

* [Natural logarithm](https://en.wikipedia.org/wiki/Natural_logarithm "Natural logarithm")
* [Exponential function](https://en.wikipedia.org/wiki/Exponential_function "Exponential function")
* [compound interest](https://en.wikipedia.org/wiki/Compound_interest "Compound interest")
* [Euler's identity](https://en.wikipedia.org/wiki/Euler%27s_identity "Euler's identity")
* [Euler's formula](https://en.wikipedia.org/wiki/Euler%27s_formula "Euler's formula")
* [half-lives](https://en.wikipedia.org/wiki/Half-life "Half-life")
* exponential [growth](https://en.wikipedia.org/wiki/Exponential_growth "Exponential growth")


