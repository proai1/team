# HowTo Work with Chat Logs

Dialog transcrips are some of the hardest NLP datasets to work with.
This challenge will test your skill.

## Take Home Challenge

You've applied for a conversation design + software developer role at a healthcare comapany.
They sent you 60 text files containing time-stamped transcripts of user conversations with their customer service chatbot.

- 60 user chat logs (text files)
- instructions: look for patterns
- Suzanna found repeated topics/questions ("patterns that repeat themselves")
- created 5 concepts like diseases and nutrition and created variables for each like breast cancer, prostate
cancer
- created a /details command/statement `if cancer send content` then would
