## Schmidt Futures Gates Foundation Research Infrastructure Incubator

We’re thrilled to let you know about the Mid-scale Research Infrastructure (RI) Incubator program, an exciting grant opportunity for research-oriented education organizations around the country. This grant program is the product of a first-of-its-kind partnership between the U.S. National Science Foundation, the Bill & Melinda Gates Foundation, Schmidt Futures, and the Walton Family Foundation. 


NSF’s Mid-scale program is designed to support projects that build the tools and capacity for cutting edge research that can lead to major scientific breakthroughs. This competition is centered on STEM education research. 

Incubator awardees will generate concrete plans for new research tools and technologies to help improve the understanding and implementation of STEM education. They might aim at (but are by no means limited to) the following objectives: 

    leveraging new technology to capture more and better data about how students learn STEM;  

    building new tools for conducting high-level research into STEM learning on education platforms;

    developing open-source software that support STEM education research and STEM learning;

    supporting research on the learning needs of students who have faced historical and current barriers to high-quality STEM education. 


Each winning team of an incubator grant will receive $500,000 for two years of funding. Funds can be used for a variety of purposes, including to convene partners, establish collaboration mechanisms, and develop innovative plans for enduring infrastructure resources that would significantly impact education research at speed and scale. 

The incubator grant is designed as the first step in implementing your research infrastructure. After the two-year incubation period, winning teams will be eligible to apply for Mid-scale proposals which can be worth up to $100 million. 

There are two separate application deadlines for incubator proposals: September 1, 2022 and March 1, 2023. NSF will conduct two webinars to answer questions about this DCL. You can register in advance for these webinars below:

    [May 26, 3-4pm ET](http://link.the-learning-agency.com/x/d?c=21881147&l=a878193c-0f98-4c1a-9ddd-8f0dada5bc61&r=0e529a9d-ce58-42b8-a83a-8f340fd95b54)

    [June 14, 3-4pm ET](https://nsf.zoomgov.com/webinar/register/WN_z3K-4n9QQluBQQ6LQEEUbQ)


Please visit this page to learn about the application process. If you have additional questions, please contact NSF at edumidsc@nsf.gov. 


Best, 

Ulrich


Ulrich Boser 

The Learning Agency
