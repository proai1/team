# Hobson's example notes

Interns can use this as a pattern for their own notes.
I will keep my notes here for the progress of interns throughout the summer 2022 internship.
I will have a section for each intern where I take notes on what I think they are working on for their internship project.

## Intern projects

### Ayobami Adebesin
      
### Bete Fulle

### Desiree Junfijiah:

### Diana  Oks

### Isaac Omolayo

### Erik Larson (and his family)

## Curriculum improvement ideas

- [ ] Better getting started exercise for the welcome message (only GitLab GUI required)
- [ ] Playground to practice manual optimization and gradient descent
