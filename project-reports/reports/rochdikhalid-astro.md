# What is Finite State Machine (FSM)?

* It is a computational model that consists of one or more states.
* It can be only in signle active state at any given time.
* An input to the machine causes a transition to the next state.
* Each state defines the state to switch to based on the user input.

![alt text](fst-diagram.jpg "FST Basic DIagram")


We will take an example of a light bulb. This object has two states, it's either turned on or turned off at any given time. If we turn on the bulb, this can cause a transition from **turned off** to **turned on** and vice versa. **ON**state defines the state to switch to which is **OFF**, and **OFF** state defines the state to switch to which is **ON**. In other words, if we are in state **ON**, pressing **ON** changes nothing, but pressing **OFF** moves the machine to state **OFF**. If we are in state **OFF**, pressing **OFF** changes nothing, but pressing **ON** moves the machine to state **ON**.

Finite means limited, so FSM is a state machine with a finite number of states. 


![alt text](fst-diagram-example.jpg "FST Example")

The diagram above shows an example of a FST. The machine starts with the state 0, the input **a** moves the machine to the state 1 and the input **b** keeps the machine in the same state. We can reach state 2 from state 1 with input **b**, and the same input leads to the state 3. The state 3 can reach state 1 with input **a**, and can make a transition to the initial state with input **b**.



# Astrobot Diagram

## Opening message

Hi there, I'm Astro - your alien bot that comes from Andromeda to help you boost your mental health in case you are experiencing depression. I provide information about depression and how to deal with this mental health condition based on science and real-life statistics.

### Opening message - option: learn about depression

bot: will provide some info about depression
(+ question?): So, how can I help you?

options: 

---
**some depression stats** - here, the bot can give some real world statistics about depression. Then, the bot can ask the user a question that can redirect to the following state: **how to know if you have a depression**.

(+ question?): After you know how depression feel looks like, what would you like to do next?

options: 

**Would you like to take a session to see if you are experiencing depression?** - if the user select this option, the bot will redirect the conversation to **how to know if you have depression**.

**Would you like to know more about depression?** - here, the bot will provide more information about depression. Then, two options will be listed: (**would you like to take a session to see if you are experiencing depression?** or **it is enough for now, talk to you next time**)

**It is enough for now, talk to you next time.** - here, the bot will stop the conversation and will save in the history. The user can get back and resume or start again the conversation with these options:

options: 

**continue** - this will resume the conversation.
**start again** - The bot will start the conversation with the opening message.

---
**how to know if you have a depression** - here, I'm looking forward to find an article that can explain the signs of this mental health condition so that I can figure out kind of quiz by which this state can be built with to see whether the user is experiencing depression or not. Later, in case of having depression, the user will redirected to the following state: **is depression treatable**.

Links to a depression test: 

https://screening.mhanational.org/screening-tools/depression/

https://www.depressedtest.com/

https://psychcentral.com/quizzes/depression-quiz#3

https://www.psychologytoday.com/us/tests/health/depression-test

https://rogersbh.org/depression-quiz

https://psymed.info/clinical-depression-test

https://www.online-therapy.com/depression/test

I'm thinking about making my own quiz based on the examples above to run **how to know if you have depression** with purpose.

Then, the bot will calculate the result of this test to see whether you are experiencing depression. If it's so, the bot will give the the user two options, one to **tackle my depression thoughts** and other to know if depression is treatable with **is depression treatable**.


**is depression treatable** - here, the bot will probably give some insights about depression based on science and real-world examples. Then, some options can be displayed. If the user choose to go for a "tackle depression" option, the bot will redirect the whole conversation to the **tackle my depression thoughts** main state. Otherwise, the user can stop the conversation by this option, **Thank you, get back to you next time** which works similarly to **it is enough for now, talk to you next time**.

The dialogue can have some options for the user to quit or stop the conversation.

### Opening message - option: tackle my depression thoughts

here, I'm trying to find a methodology or technique similar to what we've seen in qary that is based on **Burns** approach.

#### Start with experience
1. career
2. situation or event

1. Thinking about something like "Burns" approach to include it in this state, but we can customize it to fit the bot topic and make it little bit conversational.

2. Otherwise, we can give freedom to the users to express their feelings. The bot can open up the state with a simple question like "express your feelings". Then, the bot can redirect people to tips, and advices which can contain some real-world examples. When the user expresses his/her feelings, he/she needs to include some words or intents that refer to that specific emotion so the bot can recognize this and classifiy the intent to the appropriate response.


### Opening message - option: ask any question




## Bot diagram (in progress)

![alt text](astro-diagram-in-progress.png "Astro Diagram")





depression resources:

How To Deal with Depression: Tips, Techniques And Finding the Right Treatment
https://www.betterhelp.com/advice/depression/how-to-deal-with-depression-tips-techniques-treatment/


Tips to Manage Depression
https://adaa.org/understanding-anxiety/depression/tips

Depression Information & Support
https://screening.mhanational.org/depression/

---

# Everything about qary astro skill



Today, I tried to identify the bugs we have in qary life skill so that we can find a way to debug it before I created the astro skill. The following applies to all states, I tried to switch to these branches (feature-conda-environment, main, hobs, and kalika) and the same intent recognition issues have occurred.

* In the `next key`, every state is recognized when you type nothing or no intent is detected. The bot will redirect you to the state that is assigned to `next` as a value (eg. `next: RECOGNIZE` ).

* In the `next_condition` key, **RATE** is the only state that has been recognized and just the first item of it (eg. _mad_) that has been detected. However, if I moved _mad_ to the second place and put _sad_ for example as a first item, the _sad_ intent will be recognized and _mad_ not.

* The same applies to all states, the first item of `next_condition` is always what is recognized.

* Going back to __welcome__ state, I changed the intents of **RATE** to some words like _miserable_, _sad_, _terrible_, and _depressed_. The bot could recognize all these words in different places, and the word _awful_ that is not already included in the list is identified and the bot redirected me to the **RATE** state.

* Always in the `next_condition` key, I tried to move the **RATE** state that includes the new mentioned words to the second place and I put the **EXTEND** state in the first position with new words like _happy_, _joyful_, and _glad_. What happened made my day, the bot didn't recognize the **RATE** even with those new words. I tried to type one word from **EXTEND**, then the bot redirected me to the **EXTEND** state.

For more details, here is the [link](https://gitlab.com/tangibleai/qary/-/blob/rochdikhalid/src/qary/data/astrobot/astro.yml) to **astro.yml** in **qary** repository.

```
from qary.etl.dialog import *
import os
TurnsPreparation(os.path.join(DATA_DIR, 'life_coach/burns-cognitive-distortion-emotion-journal.v2.dialog.yml'))
datafile = os.path.join(DATA_DIR, 'life_coach/burns-cognitive-distortion-emotion-journal.v2.dialog.yml')
turns_input = load_dialog_turns(datafile)
prepper = TurnsPreparation(turns_input)
prepper.prepare_turns()
```

Intent recognition issues I encountered:

* It couldn't recognize intents that moves the bot to **RATE** state.
* It couldn't recognize intents that moves the bot to **RESOLVE** state.
* It couldn't recognize intents that moves the bot to **FINISH** state.
* It couldn't recognize intents that moves the bot to **EXTEND** state.
* It couldn't recognize intents that moves the bot to **RECOGNIZE** state. Sometimes, it works just in case the user types nothing (this is because of None condition).
* It couldn't recognize intents that moves the bot to **WELCOME** state.





---

# Part 1 - getting started with your first Rasa bot

## Introduction
Rasa is an open source chatbot framework to create and automate conversational applications. It provides several features to make you develop your bot in easy way. With Rasa, you can generate your training data, train your model, run tests, and connect your bot to APIs and different messaging channels. 

## Building from source
Because you are a software engineer, it is preferable to use the development version of Rasa open source. You can follow the instructions below to setup Rasa properly in your local machine.

### Create a conda environment
First, let's create a new conda environment based on Python 3.7 because Rasa and Tensorflow are more compatible with this particular version according to [Rasa Documentation](https://rasa.com/docs/rasa/). Then, we can use poetry package manager to install the dependencies needed to run Rasa properly.

```console
$ conda create -n rasaenv 'python>=3.7,<3.8' poetry
```
Note that you must activate your new conda environment to install Rasa and its dependencies in the right place. To activate a conda environment, use the following command:

```console
$ conda activate rasaenv
```

### Clone the Rasa source code
Create a new directory in your local hard drive where you can clone the Rasa source code. From your terminal, don't forget to go to the directory that was just created:

```console
$ mkdir ~/code
$ cd ~/code
$ git clone git@github.com:rasahq/rasa
$ cd ~/code/rasa 
```

### Run the peotry package manager
To install rasa in the right environment, make sure your conda environment is activated to run the peotry package manager. Poetry comes with a bunch of python packages like Tensorflow that Rasa needs for training.

```console
$ poetry install
```

Note that some machine learning algorithms require additional python packages to be installed, this [page](https://rasa.com/docs/rasa/installation) on Rasa documentation can help you configure those dependencies.

## Create a Rasa project
Any Rasa project contains some YAML files that forms the basis of skeleton conversation design of any bot. So, you can create a new directory in where you cloned the Rasa source code to hold your Rasa chatbot projects:

```console
$ mkdir ~/code/rasa-projects
$ cd ~/code/rasa-projects
```

To initialize your Rasa project, the `rasa init` command comes with a list of questions to enter a path for your project, and train your intial model. After you run the mentioned command, give a name to your project at the first prompt so Rasa can create a structure for your chatbot. Then, you can accept all the prompts except the lastest one in case you don't want to run your bot at the moment.

```console
? Please enter a path where the project will be created [default: current directory] my-first-rasa-chatbot                   
? Path '/home/code/rasa-projects/your-first-rasa-chatbot' does not exist 🧐. Create path?  Yes 

Created project directory at '/home/code/rasa-projects/my-first-rasa-chatbot'.
Finished creating project structure.
? Do you want to train an initial model? 💪🏽  Yes                                                                             
Training an initial model...
The configuration for policies and pipeline was chosen automatically. It was written into the config file at '/home/code/rasa-projects/my-first-rasa-chatbot/config.yml'.
Training NLU model...
...
Core model training completed.
Your Rasa model is trained and saved at '~/code/rasa-projects/my-first-rasa-chatbot/models/20220121-114355-burgundy-moat.tar.gz'.

? Do you want to speak to the trained assistant on the command line? 🤖  (Y/n) No

Ok 👍🏼. If you want to speak to the assistant, run 'rasa shell' at any time inside the project directory.
```

### Start with Rasa playground data
When you run the `run init` command, Rasa created a project structure for your bot that includes some dependency files like **config.yml** and some directories includes **data** and **models**. So, let's create a new YAML inside **data** directory, and name it something like **my-first-training-data.yml**. 

```console
$ cd ~/code/rasa-projects/my-first-rasa-bot/data
$ touch my-first-training-data.yml
```

Rasa has a nice conversation design that can get you start creating the first training data for your bot. You can go to [Rasa Documentation Playground](https://rasa.com/docs/rasa/playground) to build your assistant bot with an interactive guide. To keep things simple at the moment, let's copy the content of this [file](https://gitlab.com/rochdikhalid/blog/-/blob/main/data/training-data.yml) immediately into your training YAML file. Then, don't forget to import your intents in the **domain.yml** as we did [here](https://gitlab.com/rochdikhalid/blog/-/blob/main/data/domain.yml)

### Train your bot
Next, to run your first model using the training data you just created, you can use `rasa train` command that comes with two attributes. The `--data` used to enter the path where the training data is located, and the `--config` to define the **config.yml** that your model will use to make predictions based on user inputs.

```console
$ cd ..
$ rasa train --data data/my-first-training-data.yml --config config.yml
```
The training command above will help your Rasa bot classify what user is saying into the right intents, in a way your bot will be able to recognize different user inputs even with words that are not included into your training data file. The training process will ended up by saving your model to the **models** directory of your project.

### Run your bot
Lastly, you can run your first Rasa bot using the `rasa shell` command. Try to test manually your bot by typing some inputs that are not the same as you typed into the training YAML file. Feel free to stop the conversation at anytime you want using the `/stop` command.

---

# Part 2 - A closer look at Rasa components

## Introduction

In the previous tutorial, we created a very simple bot to launch our Rasa journey. In this tutorial, we will take a closer look at the basic components that Rasa framework comes with when we initialized our first bot. Before we dive into this, let's understand first how this framework operates in the background.

## How Rasa framework works?

Rasa framework contains two libraries, **Rasa Core** and **Rasa NLU**. The first library represents the natural language understanding of the framework, it can be the intrepreter that classifies intents and extracts entities based on machine learning techniques in order to make the bot able to understand the user inputs. The second library is the engine of Rasa, it is operated by a deep learning neural network called LSTM (Long Short-Term Memory) to teach the bot how to make responses and give appropriate replies to the user. 

![alt text](../data/rasa-diagram.png "Rasa Basic Diagram")

The story begins when the user sends an input to the bot. Here, the **Rasa NLU** will interpret the input with the aim to classify intents and extract entities and other particular information. Next, **Rasa Core** comes to manage and maintain the conversation state using the following parameters to execute the action needed and send a message to the user:

* **Tracker** is used to keep track of the conversation history and save it in memory. 
* **Policy** is the decision maker of Rasa, it decides on what action the bot should take at every step in the conversation.
* **Featurizer** with the help of **Tracker**, it generates the vector representation of the current conversation state so the action can pass later a **Tracker** instance after every interaction with the user.

## Rasa Components

When you run `rasa init` command, the framework creates an initial structure for your project. With this structure, developers has the freedom to customize their bots settings to fit their own purposes. Therefore, each Rasa project contains the following structural components that form the basic skeleton of your bot:

### Data

The most essential part in the process of building a bot is the data by which your bot will be based on to learn how to recognize the user inputs. Rasa comes with different types of training data to train your bot, they varie in terms of structure and functionality and can be determined by the following top level keys:

#### NLU (nlu.yml)

The purpose of using **NLU** (Natural Language Understanding) is to identify and extract the structured information from user inputs that can include **intents**, **entities**, and other extra information like **regular expressions** to improve the bot performance. The following shows a basic example of how **NLU** is structured:

```
version: "3.0"

nlu:

  - intent: greeting
    examples: |
      - hey
      - hi
      - hello
      - hola
      - hallo
  ```

#### Stories (stories.yml)

It represents and manages the conversation flow between the user and the bot. Here, the user inputs are defined as **intents** which are just the identifiers you defined in nlu.yml. The bot responses are defined as **actions**. In other words, **Stories** define the dialog _tree_ . The dialog _tree_ is actually a _graph_ - connections between bot states. Each state is something the bot will do or say. The edges or transitions between states are the actions or statements the user makes.  So the **stories** create the design of the conversation that determines the steps of the entire interaction. Rasa stuctures **stories** in the following manner:

```
version: "3.0"

stories:

  - story: greet
    steps:
    - intent: greeting
    - action: utter_greeting
    - intent: howrey
    - action: utter_howrey
```

#### Rules (rules.yml)

Another type of training data called **rules**, designed to handle some specific **intents** in your conversation dialogue always with the same response. Meaning that **rules** are not able to respond appropriately to unseen user inputs as **intents** and **stories** do. This is how **rules** structured in Rasa:

```
version: "3.0"

rules:

  - rule: say the following at anytime user says something like goodbye
    steps:
    - intent: goodbye 
    - action: utter_goodbye
```

### Actions

Too simply, **actions** is where you can automate the bot to respond to the user inputs based on **intents** and **stories**. Rasa comes with five types of **actions** you can use to interact with user messages:

* Responses
* Default actions
* Custom actions
* Form actions
* Slot validation actions

You probably noticed one type of these actions above in **rules** and **stories** examples, it starts with `utter_`. Anyway, don't stress your mind with that at the moment, we will discuss each of these actions later in the next blogs.

### Domain (domain.yml)

**Domain** is the environment that meets the bot components, there you can import your **intents**, **entities**, **actions** and other stuctured information and settings together to configure how the bot should operates. The following gives an example of how **domain** file looks like:

```
version: "3.0"

intents:
  - greeting
  - howrey
  - positivemood
  - negativemood
  - whory
  - howoldy
  - wherey
  - whatint
  - thankyou
  - goodbye

actions:
  - action_restart

responses:

  utter_greeting: 
    - text: |
        hello there, how are you?
      buttons: 
      - title: "great"
        payload: "/positivemood"
      - title: "super bad"
        payload: "/negativemood"

  utter_howrey: 
    - text: |
        pretty good, what about you?
    - text: |
        awesome as always, how are you?

  utter_positivemood: 
    - text: |
        awesome.
    - text: |
        great.

  utter_negativemood: 
    - text: |
        no worries, life happens.
    - text: |
        don't worry, this is temporary.

  utter_whory: 
    - text: |
        I'm astro.
    - text: |
        You can call me astro.

  utter_howoldy: 
    - text: |
        quite young.
    - text: |
        I'm still young by your standards.
    - text: |
        younger than you.

  utter_wherey:
    - text: |
        million light years away.
    - text: |
        I come from andromeda.

  utter_whatint:
    - text: |
        I am interested in a wide variety of topics, and read rather a lot.
    - text: |
        I'm curious about everything and I love reading useful topics.

  utter_thankyou:
    - text: |
        you are welcome.
    - text: |
        my pleasure.
    - text: |
        de nada.

  utter_goodbye: 
    - text: |
        glad to chat with you, come back again.
    - text: |
        bye, take care.

session_config:
  session_expiration_time: 60
  carry_over_slots_to_new_session: true

```

### Config (config.yml)

As the name suggests, this configuration file determines some dependencies needed to train your model. It allows you to customize and adjust the pipeline and policies to make appropriate predictions. Also, this file comes with a default model configuration in case you don't need any adjustment.

### Models

This directory saves your trained models into **.gz** format, here is an example of a trained model:
**20220131-160359-teal-holder.tar.gz**.

### Tests (test_stories.yml)

Rasa allows you to validate and test the conversation flow of your bot to see how is able to generalize to unseen conversation paths by running **test stories**. The following shows an example of a **test story**:

```
version: "3.0"

stories:

  - story: greet
    steps:
    - user: |
        hello
      intent: greeting
    - action: utter_greeting
    - user: |
        how are things?
      intent: howrey
    - action: utter_howrey
```

### Endpoints (endpoints.yml)

This file contains utilities that can help your bot be exposed to a server to run your custom actions, keep track of the conversation and store it in memory or SQL database, and stream all conversation events.

### Credentials (credentials.yml)

This file contains detailed credentials for different messaging channels which your bot is using.

## Training data format

Rasa uses **YAML** to manage your training data, including **nlu**, **stories**, **rules**, and **domain**. The training data can be combined into one single file, while **domain** can also be split into multiple files. Note that the top level key (eg. `nlu`, `stories`) should be always determined at the beginning of each training data type.

## What's next?

Hopefully this tutorial helps you understand your Rasa project structure and what each bot component is used for. In the next blog, we will learn in details how to create a **NLU** data for our bot with other structured information like **entities** and **regular expressions**.

---

# Rasa: Keep track of your conversation history 

Rasa has its own way to save the conversation history like qary, This is possible in Rasa framework with an object called **tracker_store**. The following is what I found from Rasa open source documentation:

## What is "Tracker Store"?
I order to save the conversation history in Rasa, there is an object called **tracker_store**. It keeps track of your conversation history as you interact with your bot. The conversation will be stored in the memory even **Tracker Store** is not configured in your Rasa project. However, the conversation history will be lost if your restart your Rasa bot/server.

You can learn more about this topic [here](https://rasa.com/docs/rasa/tracker-stores)

Therefore, Rasa comes up with other solutions to this issue by storing your bot's conversation history in SQL database. The example below shows how to configure  **Tracker Store** using PostgreSQL:

### PostgreSQL Setup (Ubuntu)
Before we install PostgreSQL, it's preferable to update your Ubuntu packages first so that you can install the lastest version of PotgreSQL properly in your machine. 

```console
$ sudo apt-get update
$ sudo apt-get install postgresql postgresql-contrib
```
After you install POstgreSQL, run this command to make sure that it is running:

```console
$ service postgresql status
```
Then, let's setup our default user **postgres**:

```console
$ sudo su postgres
```
You can run PostgreSQL shell to manage your database (you close the shell by typing `exit`):

```console
$ psql
```
Keep the shell running, and let's display the list of databases and database users. We will get our default user **postgres**:

```console
$ \du # list of users
$ \l # list of databases
```

Let's set password to our **postgres** user:

```console
$ ALTER USER postgres WITH PASSWORD 'abcde123'
$ # Expected output: ALTER ROLE, meaning your password is created successfully
```

You can create a new user instead of default one:

```console
$ CREATE USER alien WITH PASSWORD 'alien123'
$ # Expected output: CREATE ROLE, meaning the user is created successfully
```
If you want this new created user as **superuser**, you can assign a role attribute as following:

```console
$ ALTER USER alien WITH SUPERUSER
$ # Expected output: ALTER ROLE
```

### pgAdmin installation
First, let's add the official pgAdmin PPA and update the package list:

```console
$ sudo curl https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo apt-key add

$ sudo sh -c 'echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list && apt update'
```
Then, we can install pgAdmin4:

```console
$ sudo apt-get install pgadmin4
```
Now, you can find the pgAdmin shortcut in the application menu. After you open up the application, pgAdmin will ask you to set a master password to secure your databases. Then, go to the left panel of your application, and click right to select _create/server_. Enter the name of your server in the **General** tab (something like: pg-12) and keep everything in this tab as default. Next, go to **Connection** tab and enter your database username and password that we set before (postgres or alien) to connect to the database server.

Then, you will see your server displayed in the left panel. Click right and select _create/database_ to create a new database. Choose a name for it and keep everything as default.

### Tracker Store Configuration
Now, let's set up **tracker_store** with PotsgreSQL as follows:

1. Add required configuration to the **endpoints.yml**:

```
tracker_store:
    type: SQL
    dialect: "postgresql"  # the dialect used to interact with the db
    url: ""  # (optional) host of the sql db, e.g. "localhost"
    db: "rasa"  # path to your db
    username:  # username used for authentication
    password:  # password used for authentication
    query: # optional dictionary to be added as a query string to the connection URL
    driver: my-driver
```
2. Then, run your bot using your new SQL update:

```console
$ rasa shell
```
3. Now, refresh your database table as you interact with your bot to see the conversation history recorded.
