# Skills Checklist

You'll learn these skills over the 10 weeks, but not in this order.
Think of this as what to expect on your final exam.
You'll learn much more than this.
But these are the skills that I find most professional software development teams expect from a junior developer.
 to see if you have what it takes to contribute to a typical Python software development team.

* 01: Programming (Python):
    * [x] install packages from pypi
    * [x] importing modules within packages like sklearn and Pandas
    * [x] install packages from source code (--editable)
    * [x] install and import packages from gitlab
    * [x] conditions: `if`, `else`
    * [x] loops: `for`, `while`
    * [x] functions (args, kwargs, `return`)
    * [x] classes: `class`, methods, attributes
    * [x] scalar numerical data types (`int`, `float`)
    * [x] sequence data types (`str`, `bytes`, `list`, `tuple`)
    * [x] mapping (`dict`, `Counter`)
    * [x] sets (`set`)
    * [x] create stackoverflow account
    * [x] reading Tracebacks and error messages
    * [x] getting help (`help()`, `?`, `??`, `[TAB] completion`, `vars()`, `dir()`, `type()`, `print()`, `.__doc__`)
    *
* 02: Shell (Bash)
    * [x] bash nlp (`wc`, `grep` [stanford.edu/class/cs124/lec/textprocessingboth.pdf])
    * [x] navigate directories (`cd`, `pwd`, `ls -hal`, `/`, `~`, `*`, `$HOME`)
    * [x] manipulate files + directories (`mkdir ~/code`, `mv`, `cp`, `touch`)
    * [x] work with text files (`more`, `cat`, `nano`)
    * [x] pipes and redirects (`|`, `>`, `>>`, `<`)
    * [x] processes (`ps aux`, `fg`, `bg`, `&`)
    * [x] conditions (`&&`, `||`)
    * [x] tab-completion
    * [x] comments and shabang (`# `, `#!`)
    * [x] permissions (`chmod`, `chown`)
    * [x] running shell commands (`source`, `.`, `eval`)
    * [x] finding files: `find . -iname qary -size +1k`
    * [x] ssh to remote server: `ssh intern@totalgood.org`
    * [x] set up ssh keys: `ssh-keygen`, `ssh-copy-id`
    *
* 03: Git
    * [x] create gitlab account
    * [x] find and fork a project on gitlab.com
    * [x] create project in gitlab.com
    * [x] edit a file in gitlab.com (README.md)
    * [x] add a file using gitlab.com GUI
    * [x] upload a file to gitlab.com with GUI
    * [x] ssh public key in gitlab
    * [x] clone a repository from the command line
    * [x] create a merge request in gitlab
    * [x] `git branch`
    * [x] `git merge`
    * [x] resolve merge conflicts (know how they happen too)
    * [x] habitually use the `status`, `add`, `commit -am`, `pull`, `push` workflow
    *
* 04: Python Data
    * [x] loops: `enumerate`, `tqdm`
    * [x] list comprehensions to transform features: `[x**2 for x in array]`
    * [x] conditional list comprehension: `[x**2 for x in array if x > 5]`
    * [x] load csv: `df = pd.read_csv()`,  `df = pd.read_csv(sep='\t')`
    * [x] DataFrames from HTML tables: `df = pd.read_html()`
    * [x] bigdata: `df = pd.read_csv(chunk_size=...)`
    * [x] vectorized operations: `x1 + x2`
    * [x] concatenate: `pd.concat(axis=0/1)`
    * [x] `pd.Series.apply()`
    * [x] indexing `DataFrame`s with `.iloc` `.loc` and `[]`
    *
* 05: Statistics & Data
    * [x] `np.random.rand`, `.randint`, `.randn`, `np.random.seed`
    * [x] standard deviation
    * [x] error metrics (RMSE, loss)
    * [x] optimization (objective function, gradient descent)
    * [x] [68-95-99.7 Rule](https://en.wikipedia.org/wiki/68%E2%80%9395%E2%80%9399.7_rule)
    * [x] histograms
    * [x] probability distructions (normal, log/Poisson)
    * [x] distribution skew
    * [x] data scedasticity and stationarity
    *
* 06: Data Science
    * [x] conventional approach to DS: ETL, EDA, modeling, evaluation, explanation (insight)
    * [x] agile data science: engineer 1 feature, model, evaluate, engineer 1 more, model, evaluate, ...
    * [x] at least 6 kinds of features (2 fundamental, 4 "special" information-rich kinds of data)
    * [x] how to select a target variable
    * [x] 4 kinds of data science problems and example applications for each
    * [x] Chosing a model type: [sklearn decision flow chart](https://scikit-learn.org/stable/tutorial/machine_learning_map/index.html)
    * [x] Your "go-to" models for regression and classification: Lasso|Ridge, LogisticRegression
    * [x] make predictions/estimates with pretrained machine learning model
    * [x] training/fitting a machine learning model on a training set
    * [x] validating/evaluating/testing a model on a validation set or test set
    * [x] overfitting: how to detect it, and what to do about it
    * [x] class bias: how to detect it, and what to do about it
    * [x] model performance metrics: `R**2 score`, RMSE, F1-score, accuracy, precision, recall
    * [x] how is time series forecasting different from modeling tabular data
    * [x] ARIMA and how to implement it in python or using sklearn's LinearRegression
    * [x] confusion matrix
    * [x] residual plots and how to use them to improve your model (using heteroscedasticity)
    * [x] scatter plots and how to interpret them
    * [x] what kinds of problems are neural networks good for?
    * [x] what are CNNs for? (example applications)
    * [x] what are RNNs (LSTM, GRU, Transformers) for? (example applications)
    * [x] what is BERT, GPT-2
    *
* 07: Networking
    * [x] python `requests` package and `.get`, `.post` methods and `stream=True`
    * [x] `pandas.read_html()` function
    * [ ] python `BeautifulSoup4` package
    * [x] address bar in browser for HTTP GET requests with arguments
    * [x] address bar in browser for HTTPS requests with arguments
    * [x] `wget` or c`url` for command line GET requests with arguments
    * [ ] `wget` or `curl` for command line POST requests with data (arguments)
    *
* 08: Software development Best practices
    * [x] zen of python
    * [x] style guides, linters, PEP8
    * [x] Syntax highlighters
    * [x] Anaconda auto-formatter for Sublime
    * [x] self-explanatory variable names (verbs for functions, plural nouns for lists, etc)
    * [x] [Napolean-style docstrings](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html)
    * [x] [doctests](https://django-testing-docs.readthedocs.io/en/latest/basic_doctests.html)
    * [ ] [testing](https://python-102.readthedocs.io/en/latest/testing.html) (`pytest`, TDD, `unittest`, regression
    * [ ] how to use git on a team
    * [ ] constructive [code reviews](https://youtu.be/iNG1a--SIlk)
    * [ ] patterns and [antipatterns](http://docs.quantifiedcode.com/python-anti-patterns/)
    tests)
    *
* 09: NLP and chatbots:
    * [x] at least 4 fast & easy features to extract from NL text
    * [x] at least 4 chatbot approaches: rules, search, grounding, generative models
    * [x] create a zero-order markhov chain (model) of text
    * [x] first and second order markhov chain models (Uzi's diagrams)
    * [x] information content (entropy) of a string
    * [x] algorithmic (Kolmogoroph) complexity of a string (and how to easily measure it)
    * [x] chatbot architectures (deterministic, generrative, knowledge-based)
    * [x] techniques for intent recognition: exact match, keywords, doc vectors, BERT/USE embeddings
    * [x] keyword intent recogition
    * [x] information extraction, entities
    * [x] efficient full text search (indexing)
    * [x] utterances, turns, dialog trees, dialog cycles, dialog engines
    * [x] conversation management, conversation design
    * [x] voice recognition and multimodal UI (AllenAI)
    * [x] a common way to use transfer learning for any NLP problem
    *
* 10: Business:
    * [x] Agile (standups, retros, planning, poker)
    * [ ] help stakeholders select a target variable and craft an objective function
    * [x] Introduction to Tangible AI
    * [ ] "How nonprofits use AI" webinar
    * [x] Open source principles
    * [ ] Differences between open source licenses: Hippocratic, MIT, GNU, Apache, CC-BYA
    * [ ] data privacy ethics and law (GDPR, HIPPA)
    * [ ] Common chatbot/NLP applications in the nonprofit world
    * [ ] Common chatbot/NLP applications in the for-profit world
    * [ ] The Technological Singularity
    * [ ] The Economic Singularity
    * [ ] Open source licenses and how to choose the right one
    * [ ] Beneficial AI
    * [ ] The control problem
    * [ ] The Technological Singularity
    * [ ] The Economic Singularity
    *
