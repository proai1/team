# Intern checklist

## Repositories (3)

1. [Skills Checklist](https://gitlab.com/tangibleai/team/-/blob/master/learning-resources/intern-syllabus-and-skills-checklist.md)
2. [Project Ideas](https://gitlab.com/tangibleai/team/-/blob/master/learning-resources/projects/prosocial-ai-ideas-and-projects.md)
3. [qary](https://gitlab.com/tangibleai/qary/)

You'll probably want to clone or fork the first repostitory above (gitlab.com/tangibleai/team/) so you can upload your skills checklist and share what you learn over 10 weeks.

## Slacks (3)

Connect to us on Slack:

1. https://proai.org/tangibleai-slack
2. https://proai.org/sdml-slack
3. https://proai.org/sdpython-slack-invite

## Meetings (5 weekly meetings)

Put some meetings on your calendar:

- **IMPORTANT**: Schedule a weekly 1:1 with me on my calendar [calendly.com/hobs](https://calendly.com/hobs)
- _Recommended_: [Improv Programming 1 hr, Wed 5:30](https://calendar.google.com/event?action=TEMPLATE&tmeid=N3E0Zms1OTg3amsxY2Nhb2M3YzU0MDc4YmtfMjAyMTA3MDFUMDAzMDAwWiBob2Jzb25AdG90YWxnb29kLmNvbQ&tmsrc=hobson%40totalgood.com&scp=ALL) Zoom: [ucsd.zoom.us/my/hobsonlane](https://ucsd.zoom.us/my/hobsonlane)
- optional: [Standup 10 min, Tues 10:15-10:25](https://calendar.google.com/event?action=TEMPLATE&tmeid=MWZ1djYwZnUwY28xZ3NhaGh1c2w1N3F2M3NfMjAyMTA2MjlUMTcxNTAwWiBob2Jzb25AdG90YWxnb29kLmNvbQ&tmsrc=hobson%40totalgood.com&scp=ALL) Zoom ID: 88336712107, Password: Kw8y6t2dUR
- optional: [Standup 10 min, Fri 11:30](https://calendar.google.com/event?action=TEMPLATE&tmeid=bnBnNHBzNTh2bWoyZjFhbmE0dGZlNXI3dXBfMjAyMTA3MDJUMTgzMDAwWiBob2Jzb25AdG90YWxnb29kLmNvbQ&tmsrc=hobson%40totalgood.com&scp=ALL) Zoom ID: 82526004831, Password: r40ZHM0G
- optional: [Team Lunch & Social Hour, Mon 12:00](https://calendar.google.com/event?action=TEMPLATE&tmeid=MHNxNGQwYm9pZnBpYXNudGU0Mm9jazQyM2pfMjAyMTA2MjhUMTkwMDAwWiBob2Jzb25AdG90YWxnb29kLmNvbQ&tmsrc=hobson%40totalgood.com&scp=ALL) Zoom ID: 290207651, Password: tangibleai


## First Week

### Set up GitLab Account

- [ ] create gitlab account (do not click "Start Trial")
- [ ] confirm your gitlab email address
- [ ] navigate to gitlab.com/tangibleai/education and click green "Request Access" button
- [ ] send slack message on #interns channel letting us know your gitlab username
- [ ] TODO H: add the interns' gitlab usernames to Members on the `team/` project as developers

### Check off some skills in the skills checklist

There are two ways to do this, the developer way, and the GUI way

#### Developer approach

- [ ] fork or clone the [team repo](https://gitalb.com/tangibleai/team)
- [ ] if you cloned the repo, create a branch on your local machine
- [ ] use a text editor to open the text file in `learning-resources/intern-syllabus-and-skills-checklist.md` or `interns/skills-checklist/blank`
- [ ] save the file under 'interns/skills-checklist/your-name.md'
- [ ] check off any skills that you think you've mastered
- [ ] add any skills that you'd like to learn
- [ ] add, commit, and push your new file to gitlab on your branch
- [ ] go to gitlab to issue a merge request from your branch to master

#### GUI approach

- [ ] browse to the [skills checklist](https://gitlab.com/tangibleai/team/-/blob/master/learning-resources/intern-syllabus-and-skills-checklist.md) on gitlab
- [ ] click the [Edit] button or [raw] link
- [ ] copy all of the raw text within the checklist to your clipboard
- [ ] navigate to the [`interns/skills-checklist` directory](https://gitlab.com/tangibleai/team/-/blob/master/interns/skills-checklist)
- [ ] click the plus sign at the top of the page to create a new markdown file called `your-name.md` and paste the unchecked checklist into it (here are some screenshots describing [how to create files in gitlab](https://gitlab.com/tangibleai/qary/-/blob/master/docs/convoscript/README.md#creating-text-files-on-gitlab) for a different repository, `qary`)
- [ ] check off any skills that you think you've mastered by putting an x between the square brackets
- [ ] add any skills that you'd like to learn
- [ ] click the [commit] button at the bottom of the page and click the buttons that allow gitlab to create a merge request to master
- [ ] make sure you scroll to the bottom of the merge request to click the button to "request merge" or [issue merge request]


### Set up your Dev Environment

- [ ] install Anaconda
- [ ] install bash (git-bash or WSL on Windows)
- [ ] learn these bash file maniuplation commands and apps thoroughly: cd, ls, cat, find, mv, mkdir
- [ ] stretch goal: choose a text editor for your gui (sublime or pycharm) or your terminal (nano or vim) and learn how to creating, edit, and save a file
- [ ] TODO H: create a bash/filesystem quiz in qary 

