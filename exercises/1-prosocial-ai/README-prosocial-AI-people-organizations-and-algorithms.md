# Elevate Prosocial Intelligence (artificial and otherwise)

Let's do some prosocial mental exercises.
What you think about is what you do.
And what you do is what you think about.
So let's do some thinking and doing of prosocial AI support.

Research and understand what makes a statement, idea, algorithm, organization or thought leader more or less prosocial.
It can help you design better objective functions for optimization problems and craft a target variable for your machine learning model that is worthy of your prosocial objectives.
And it can help you create incentives for the users of your software and your teammates that work with you on software projects.

There's no known solution to the [AI Control Problem]().
So our only hope is to show our robot overlords (and Big Tech) that our lives (and their existence) is a positive sum game.
What's good for us is good for them.
You are going to model prosocial behavior to train AI algorithms that want to treat us better.

## 0. Concept

Think about what the word "prosocial" means to you.
Then read about what pscyhologists and philosophers think about it [on Wikipedia](https://en.wikipedia.org/wiki/Prosocial_behavior).
Think about how AI, algorithms, and machine learning models could be influenced, trained, or designed to be more prosocial.

## 1. Tools

When you're doing your research try to use tools outside the Big Tech stranglehold on influence and information.
It's hard.
Even Duck.com receives a lot of its ad and search revenue from Google.

Try to use social networks that at least attempt to be open and prosocial by providing you with an open API and low cost ad-free content.
You can search for "private open ethical objective ad-free research information retrieval" in your favorite search engine.
This will train the search engines to uprank prosocial searches and independent/smaller/nonprofit businesses.
Some of my favorite search bars are in order of preference and prosocialness:

* [Wikipedia](wikipedia.org)
* [Creative commons](wikimedia.org)
* [gitlab.com](gitlab.com)
* [Papers With Code](paperswithcode.com)
* [Stack Exchange](https://stackexchange.com/)
* [Substack](https://substack.com)
* [Open Library](openlibrary.org)
* [Reddit](reddit.com)
* [Google Scholar](scholar.google.com)
* [Twitter](twitter.com)
* [Overdrive](https://www.overdrive.com/)

And do your own searches for FOSS tools to help you keep your brain as uncluttered as possible:

* FOSS web search engine with public api
* FOSS social network with public api
* FOSS learning or education platforms
* FOSS, open data, open science, dataset management services
* FOSS, open api, fact checking services and tools
* [citizen data science](https://en.wikipedia.org/wiki/List_of_citizen_science_projects)
* International [DS competitions](https://www.kdnuggets.com/2020/09/international-alternatives-kaggle-data-science-competitions.html)
* [Distributed computing projects](https://en.wikipedia.org/wiki/List_of_distributed_computing_projects)

## 2. Concepts

Terms like "loyal ai" and how it's different from "beneficial ai" might be a good search.
Research AGI.
Find out what it is and see if you can think of narrow domain-specific examples of it in your life and society.

Do a little [Ducking](https;//duckduckgo.com) (web searching and click-holing) on some prosocial ideas, memes, and concepts:

* Prosocial AI and ethical AI
* Doughnut economics, global happiness report, GDP
* Supercooperators, evolution of altruism and spite, nice guys finish first
* The selfish gene, sociopathy
* Human compatible AI and provably beneficial AI
* Zero sum games and nonzero sum games (positive sum games)
* Positive Feedback Loops and Instability and Singularities
* Incentive structures, behavioral nudges, and
* Economic externalities, ESG investors and businesses
* Regenerative businesses, Cooperatives, Nonprofits, Social-impact businesses, B-corps
* FOSS, open source, open data, open api, public api, anonymous api
* Ad-free web search, "you are the product"
* Net neutrality
* Journalism ethics

## 3. People

Find poets, writers, thinkers, philosophers, futurists, technologists and researchers  that are worried about the AI control problem or the technological singularity or the economic singularity.
There is controversy surrounding almost all of these famous futurists and philosophers.
Even the most prosocial of our heroes are merely human, animals at the core.
That's why it's important for you to elevate people when they are behaving prosocially, to nudge and influence them into being better.

The following are some prosocial ai researchers, teachers, engineers, sociologists, and influencers that stood out with interns and mentors over the past few years: []

* [Prosocial thought leaders](https:gitlab.com/tangibleai/team/-/tree/main/exercises/1-prosocial-ai/intern-heroes-prosocial-people.yml)
* [Prosocial subreddits and users](https://gitlab.com/tangibleai/team/-/tree/main/exercises/1-prosocial-ai/potentially-prosocial-people-subreddits.yml)
* [Prosocial project ideas](]https://gitlab.com/tangibleai/team/-/tree/main/exercises/2-plan-your-prosocial-ai-project/prosocial-ai-project-ideas-plan-your-project.yml)

Try to find related, but less famous people, so that you get the most prosocial leverage for you likes.

And don't forget those names of forgotten heroes.
You can find prosocial people online by searching for these names on social networks and finding those who remember and emulate them.
If you want to "run into" people like you (or who you'd like to be), research topics that the people you want to emulate would be interested in.

- Aaron Swartz
- Chien-Shiung Wu
- Ada Lovelace
- Alan Turing
- Mahatma Ghandi
- Nelson Mandela
- Martin Luther King
-

You will probably enjoy connecting to the marginalized engineers who are getting down to work building open source packages that democratize AI and nudge it to be more prosocial. Try to follow at least 10 such people on GitLab, Reddit, Spotify, Twitter, iTunes, LinkedIn, or github -- in that order, from prosocial business to antisocial. And share your finds on Slack or in a blog post on substack.


## Organizations (algorithms) to Watch

### Antisocial Orgs

Nonprofits, schools, and orgs that loudly claim to be mission-driven are not always prosocial. The mission" is often self-serving, to create wealth, political influence, and power for the executives in charge. In fact, the largest, most popular nonprofits are virtual pawns of the US State Department or Corporations that benefit monetarily from their "charity."

- Red Cross
- Good Will
- Omdena
- Save the Children
- Hillsdale College (think tank)
- Many newsletters (disguised propaganda)
- Citizens United
- Cambridge Analytica
- Big-tech funded charities that donate services and "store" credit to those in need
- Many education and edtech organizations (College Board)

### Prosocial Orgs

And both for-profit and nonprofit organizations are very prosocial.
Here some prosocial organizations that might be good for your prosocial search.
Also, you can help these companies with SEO by searching for them in [Duck.com](DuckDuckGo.com) (or google, if you must). Finding them among the other distractor ads and click on the one for the domain name listed below.

- Insight Out (insightout.community)
- Free Geek (freegeek.org)
- Data Kind (datakind.org)
- Dimagi (dimagi.com)[^coi]
- ONOW (onow.org)[^coi]
- Plan International (plan-international.org)[^coi]
- Bottom Line (bottomline.org/)[^coi]
- Open Science Foundation (osf.io)
- Free Software Foundation (fsf.org)
- Electronic Frontier Foundation (eff.org)
- [NYC Mayor's Office of Immigrant Affairs (MOIA)](https://www1.nyc.gov/site/immigrants/index.page)

### Footnotes

[^1]: Daniel Dennet's _Theory of Consciousness_ [summary](https://schneiderwebsite.com/uploads/8/3/7/5/83756330/daniel_dennetts_theory_of_consciousness.pdf) by Susan Schneider
[^2]: Susan Schneider's _Artificial You_ [Ch 12 "Alien Consciousness"](https://schneiderwebsite.com/uploads/8/3/7/5/83756330/schneider_9781107109988c12_p189-206.pdf)
[^coi]: Conflict of Interest Disclosure: MOIA, Bottom Line, Dimagi, ONOW, and Plan International are all Tangible ai Customers
