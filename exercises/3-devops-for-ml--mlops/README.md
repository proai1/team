# ML Ops

Machine Learning operations is devops (software development operations) but for machine learning models instead of code.
To be an efficient Data Scientist or Machine Learning engineer you need to be able to version control, test, and deploy your machine learning models, just as you do software.
Your ultimate goal is to fully automate the testing and deployment of your ML models and datasets.
You want to create a CI-CD (Continuous Integration, Continuous Delivery) pipeline for models and data.

## Prerequesites

Make sure you are familiar with the [devops exercises](devops/) and are successfully using CI-CD for your code before diving into these ML Ops exercises.   

- [devops exercises](devops/)

## Exercises

For data privacy and access to HPC (High Performance Computing) compute resources like GPUs to train your model you may want to set up a VPN to connect to servers together.

- [setting up a vpn](Algo Deployment to DigitalOcean Droplet.md) 

## External resources

- [MLflow](https://mlflow.org/) -  FOSS machine learning lifecycle management platform
