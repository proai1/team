## 1. Self-assessment

Google asks this question of any technical interviewee.
Getting your opinion of yourself allows them to ask you questions you are unprepared for, so they can see how you think.
They can also find out if you are prone to overconfidence or an inflated ego.
Any of the candidates higher ratings should prompt the question: "How do you know?"

> Rate yourself on each of these skills from 0 to 5. Use the following 5 star scale:  
>   0: hardly heard of it  
>   1: competent, need help now and then  
>   2: proficient, work independently  
>   3: your peers come to you for advice  
>   4: expert outside your workplace  
>   5: published author, lead developer on popular project  

* POSIX (bash, zsh, sh, Linux, Ubuntu, Centos, ArchLinux)
* Devops & MLops
    * git, lfs, and dvc 3
    * CI/CD, TDD, DDD 3
    * scale/bigdata (hadoop, spark) 3
    * data engineering (ERDs, SQL, NOSQL, GraphQL) 3
    * Virtualization, Containerization 3
    * HPC (Hadoop, RocketML, Spark) 2
    * Automatic ML (Data Robot, AutoML) 2
    * Data Science 4
    * NLP 4
    * Recruiting & hiring 3
* Computer science (OOP, map/reduce, recursion, Functional, Big O, patterns/anti-patterns)
* Data Science (over/under fitting, P-value, confidence interval, cross-validation, class imbalance)
* Deep Learning (CNNs, RNNs, LSTM/GRUs, transformers, attention, minibatches, early stopping, lottery ticket hypothesis)
* Recruiting/hiring/managing people
* Agile Project Management (Scrum, Kaanbaan, standups)

## 2. Systems Thinking

> How would you architect our system?

* How many users do you anticipate? This quarter, this year, next year.
* What is your uptime/reliability requirement or goal?
* What are your margins? How much revenue do you expect per web "transaction" (message, site visit, request, signup)

You want them to ask you questions and think broadly, laterally, and suggest and compare multiple possible approaches. 
If they don't ask you about your problem or product and they act like there's only one right answer, that's not a good answer.

## 3. UX problem solving

> Say our users don't like to use SMS and we want to add buttons and other smart phone app features. What should we do?

* What are the most popular messaging apps among your expected users? What's App? Facebook? Telegram? Signal? Snap chat? Wee chat?
* What features are must haves for your messaging platform?
* What are the per-message costs for each platform?
* What are the development costs to automate messages over that platform? (open APIs like Telegram are easiest, proprietary APIs like Facebook and Weechat and Snap chat are hardest)

* open source: seq2seq trans, m2m100
* accuracy
* put push instead of post

## 4. Priorities

> If we received 1M funding what would you recommend we do with it?

* Recruit talented developers
* Purchase COTS tools and products that get us as close to an MVP as possible.
* "Buy" some initial customers by giving them something of value in exchange for helping you stress test your product.

## 5. Make/buy

> How would you build a webapp to translate from English into Amharic?

SAAS translation services like Google Translate vs open source translation models:

* [Yandex Translation](https://tech.yandex.com/translate/) ($6-$15)
* [Microsoft Azure Translation](https://azure.microsoft.com/en-us/services/cognitive-services/translator-text-api/) ($10)
* [IBM Watson Translation](https://www.ibm.com/watson/services/language-translator/)
* [SYSTRAN.io Translate](https://platform.systran.net/reference/translation)
* [MyMemory](https://mymemory.translated.net/doc/spec.php)

> Which one would you choose from the list above?

* what are their accuracies for your domain (legal) and languages?
* what are their latency, throughput?
* what are their security and privacy policies?
* how reliable are they?
* what are their hidden costs?
* "I wonder what you and your customer might care about most, accuracy, speed, price, reliability?"
* "I wonder how much effort it would be to connect to the Azure API?"
* "And what about Lock-In?"
* "What about data privacy on Google Translate?"

## Task Prioritization

> What should our deep learning pipeline look like?

* How accurate does your machine learning model need to be to satisfy a customer need?
* Do you have enough labeled data to train and validate your model?
* Do you have experts that can label data for you?
* Can your customers label your data once the system is live?
* Do you have beta testers willing to help you label your data for free?

## NLP

> If you had a dataset of translation pairs (pairs of sentences that have been correctly translated), how would you measure accuracy?

* Substitution distance
* Edit distance
* Hamming distance
* Embedding cosine distance
* TFIDF cosine distance
* [Rouge-1/2/L/S/SU/W](https://en.wikipedia.org/wiki/ROUGE_(metric))
* [BLEU](https://en.wikipedia.org/wiki/BLEU)

## Creative NLP:

> Can you think of any creative ways to evaluate translation accuracy without having an expert translator review our data?

The right answer here is to use "round trip" translation accuracy.
You can translate from English into Spanish and then back to English to see how good the Spanish translation model is.
And you don't even have to be fluent in English., without
And at a deeper level you can find out if they understand the concept of "context" and "statefulness" in an API.
One way to test it is to imagine sending several sentences from the same document right after each other to the API.
You can then see if the API is stateful and if the translation model takes into account "context" by improving the translation for later sentences in the same document.

## Continuous Improvement:

> How should we evaluate your performance at our quarterly performance reviews?

* Quantitative metrics like business performance, employee productivity?
* What are some good metrics for evaluating engineers?

## Security Systems thinking

> What are the 3 legs of IT security?

* physical
* network
* procedural

## Data-driven meritocracy:

> How can we ensure that those metrics are fair?

You want them to talk about measures of employee flexibility and curiosity and ability to fill gaps. You want to reward backend developers that step up and help out when the frontend developers fall behind. Or you want your frontend developers to help out with conversation design, marketing, and graphic design if need be. At a small company, everyone needs to be able and willing to do everything.

## Devops

> Tell me about the concept of  "infrastructure as code"?

## Devops tools

> What is your favorite infrastructure orchestration tool?

- Chef
- Salt Stack
- Ansible
- Puppet Bolt
- Terraform Helm
- Kubernetes
- Docker

## Security Ops

> Where do you store credentials for your web applications and cloud services?

* Bitwarden, lastpass, 1password
7. Data Privacy

> How do you manage employee access to customer data

> Have you heard of any of these acronyms or terminology:

* CAT: Computer Assisted Translation
* TMS: Translation Management Systems
* MT: Machine Translation
* STT/STT: Speech to Text and Text to Speech
* gettext (industry standard library for internationalization)
* i18n vs tl8n (or #xl8): internationalization, translation
* interpreter vs translator

## Related FOSS tools

There are production-quality open source tools and free APIs for things like

* OCR: [Tessrract](https://github.com/tesseract-ocr))
* TTS: [leon](https://github.com/leon-ai/leon)
* Speech recognition: [`speech_recognition`](https://github.com/Uberi/speech_recognition)
* avatar animation
* Geocoding: Pelias or http://Geocode.Earth):


Before you reveal your interest in security find out if your CTO has it at the top of their mind:

> What kind of training to you expect your developers to go through before being given access to your system?

- GDPR/HIPPA compliance training
- company privacy policies and EULAs
- IT security policies


## Security

> What are some of the unique challenges of working with remote employees? How do you deal with them.

- data security
- physical security
- security training
- communication
- morale
- security
- HR legal compliance
- layered security (VPN + server RBAC)

> How do you share passwords and tokens between team members?

- password management tools (lastpass, bitwarden, 1password)
- on paper somewhere in a locked cabinet or safe (master passwords, recovery keys):
    - password managers
    - admin email accounts
    - Cloud services (Google, AWS, DigitalOcean, Dropbox)
    - IT security and monitoring services
    - financial reporting services

> What tools do you use for alerting and Monitoring on your web applications? What about mobile applications?

> What is your protocol when a security alert is triggered?

> What is your protocol when you confirm a security incident or customer data breech?

> What are some creative ways to harden your networks?

Counterintuitively, the right answers rely on a bit of openness, which is uncomfortable for many IT managers from the "Enterprise" world:

* Offer an internal bug bounty.
* Announce a public bug bounty.
* Use open source monitoring, networking, and crytpograph software.
* Open source your frontend and backend webapp software.

> What level of security do you strive for in your systems?

If they say "what do you mean by security level", turn it back on them by asking something like:

> How do you decide when your system is secure enough?

And if you have to, spell it out for them:

> How do you measure security? Is it possible to quantify security? What is a good quantitative measure of security that you use? What are the "units" of measure for the security of a system?

Like uptime and cost there are all sorts of things you want your CTO to be measuring. And the most important is the security of your system.  My favorite measure of security is the $ cost of hacking your system. Every system is hackable, it's just a matter of how motivated a hacker is. So you want to make sure your system's aparent value to hackers is far lower than the cost for them to hack it. So if you are a $1M business, you want it to cost the hackers $10M to hack you.

A great CTO will wax on and on about how he looks at the cost to hire IT private investigators and pentesters and ethical hackers to hack an iPhone connected to your system, or a Django website, or a Elastic Search server. He can then use that as a baseline for how much it would cost to get into your system. The cheapest possible way to get in, that's the cost of getting into your system. That's a $ measure of your security level.

Some less desirable, more inaccurate options for security metrics might be:

* average number of bugs fixed per month (bug squashing velocity of your team)
* number and severity of outstanding issues filed by users & developers
* test coverage for your software
* automated pentest failure rate (tracked like downtime)
* server downtime
* number of suspicious events or "close calls" on your servers

Many of these metrics can be misleading, so be careful.
Meausring security is like measure code health or software quality.
It's hard.

## Digging Deeper on Devops

> tell me about the concept of  "infrastructure as code"?

what is your favorite infrastructure orchestration tool

- Chef
- Salt Stack
- Ansible
- Puppet Bolt
- Terraform
- Helm
- Kubernetes
- Docker

> where do you store credentials for your web applications and cloud services

> how do you manage employee access to customer data

> how do you stratify data

- who needs to access what (privacy+security)
- performance (colocation)
- not sharding for performance

> what is your favorite database monitoring tool. what do you use it for.

- monitoring access/security
- monitoring performance
- monitoring integrity

## Up to Date

> tell me about a recent high profile security breach and what you learned from it

- solarwinds 2020, 1000+ companies
+ master password for solarwinds products

- uber 2017, 57M users
+ aws key in 3rd party sw repo

- brit airways 2018, 400k customers

- Microsoft 100+ large companies
+ LDAP breech

- Mocrooft Hotmail backdoor 
+ blank password? letter e or h for password

## Balance

> Where do you stand on public vs private cloud?[^2]

> Are you more of a Bayesean or a Frequentist?

> Where do you stand on open source vs closed source as a business model?[^3],[^4]

> Functional vs OOP?

> Python vs Java vs Javascript ;)

## Footnotes

[^1]: [AI is an Ideology not a Technology](https://www.wired.com/story/opinion-ai-is-an-ideology-not-a-technology/) Take any idea and add water, I mean AI.
[^2]: [Inherently Hybrid Question](https://www.wired.com/insights/2014/03/inherently-hybrid-question-public-vs-private-cloud-answer-yes/)
[^3]: [Go Green](https://www.wired.com/story/go-green-energy-industry-open-source/) Open source in the ESG sector
[^4]: [Open Source Software -- The complete Guide (Wired)](https://www.wired.com/story/wired-guide-open-source-software/)
