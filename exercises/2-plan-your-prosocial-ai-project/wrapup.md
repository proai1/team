# Before you Go

Here's a checklist of things to help you to move onto bigger and better things. Your future self will appreciate it.

- [ ] 1. Push your latest project code up to a gitlab repository
- [ ] 2. Push all the data required for your project to your repo
- [ ] 3. Edit the tangibleai/team [proai.org/project-reports](proai.org/project-reports) markdown file to include a link to your report or GitLab repo
- [ ] 4. Create a merge request to  `gitlab.com/tangibleai/team` with your final report (.md, .docx, .pptx, or .ipynb)
- [ ] 5. Add your gitlab repo URL to the gitlab.com/tangibleai/team project ideas markdown file
- [ ] 6. Think about your future self coming back to your project after you've forgotten all about it -- document (in README.md) what you will need to know to get started working on your code (run it, test it)
- [ ] 7. Edit or add an idea to the the [proai.org/project-ideas](proai.org/project-ideas) markdown file (with a merge request) so future interns can build on your work)
- [ ] 8. Revisit the skills checklist to see what you've learned and get ideas on where to go from here!
