## Getting Started

* Discuss with client what dataset should be used first. 
* Load your dataset in a Pandas dataframe. 
* Discuss with client the initial version of the target variable. 
* Create your target variable. 

## Data Cleaning 
* Use functions from `tangibleai.data-cleaning` to do some of the following: 
  * Turn categorical variables into a one-hot feature
  * Create a binary column to represent if the original data was null. Then, fill out the nulls in numerical columns.


## First model 
* Create a simple model from the first batch of variables 
* Create a file in `folder` folder to track accuracy of your models  
## 
