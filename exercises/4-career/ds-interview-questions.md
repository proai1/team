2020-10-06 Statistics Interview questions


## A/B testing 

- how do you decide if group assignment is random
- what is an A/a test good for
- what do you do it your kpi is right skewed (clip, log, rank, or quantile the kpi)
- what do you do when you have 20 treatments
- hazards of letting one group peek at another?
- how do you conduct opt-in a/b test (don't let every one who opts in get the full treatment)
- what if a blog covers one of your treatments
- how to do multiple treatments/experiments with signup button (make sure exclusivity is maintained so user can't sign up multiple times)
- how would you measure latency impact on user experience (random slowdown assignment)
- max likelihood estimator MLE (method of estimating parameters of a statistical model)
- compare MAP MOM and MLE models (map: posterior from prior, mle: map with uselessly uniform prior distribution, mom: sets moment values and is usually worse than mle)
- calculate a confidence interval
- what is bias in an estimator (when expectation of model estimate is different from expectation of target)
- what is selection bias and give examples
- p value, type 1 and type 2 errors
- Pierson LSD test
- MANOVA means test
- bobferroni p value correction
- 







