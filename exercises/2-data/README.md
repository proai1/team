# Data Wrangling

Data Science usually involves an extract-transform-load (ETL) process and exploratory data analysis (EDA). 
Together these steps are often called data wrangling or data cleaning.

1. ETL: [Find some good data](./datasets.md)
2. EDA: [Wrangle it into a single table](./data-wrangling.md)
3. Feature engineering: Clean it and convert it to numerical data for machine learning
