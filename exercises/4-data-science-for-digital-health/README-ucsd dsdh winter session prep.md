+2021-10-16 ucsd dsdh winter session prep

## [Australia Data Science Education Institute](https://adsei.org/blog/)

Dr. Linda McIver teaches skepticism about data

Medical horor stories

- transvaginal mesh surgery recommended for women without any scientific evidence and no tracking of outcomes [guardian](https://www.theguardian.com/society/2017/aug/31/vaginal-pelvic-mesh-explainer)
- pain correlation with hip diagnoses and no statistically significant difference in MRI scans between people with and without hip pain [ADSEI](https://adsei.org/2021/07/20/evidence-based-medicine/)
- thalidamide
- [datasets](https://adsei.org/datasets/)
