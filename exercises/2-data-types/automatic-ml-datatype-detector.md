# Automatically detect data science data types
​
## Top 10 Kinds of DS Data

You can probably think of 10 different kinds of data you will deal with in your life.
You will have a different set of steps that you do for each of these data types.
The feature extraction or feature engineering process (or automatic algorithm) will be different for each.
So this is the first thing your automatic machine learning pipeline will have to do, classify your data.

Notice that the data science data type (numerical, categorical) is different from the computer science data type (float, str).
For example a `str` could be used in your code or your dataframe to hold categorical data or natural language data.
The only way to know is to read each one ... or build an automatic natural language detector.

1. numerical
2. categorical (red, green, blue)
3. tags (nonexclusive categories such as [fiction, science, science-fiction, fantasy, liked])
4. ordinal (small, medium, large)
5. date/time/datetime
6. location (lat/lon, address, zip code, etc)
7. natural language (a description or note with high cardinality for that dataset)
8. image
9. video
10. audio (or time series)

You want a function that can detect each of them. 
And then you'll build another specialized feature extraction function for each data type you have to deal with in Data Science.
That will gi e you a pipeline that can handle all of them automatically.
But as with all Agile project you start with the simplest possible small task first, detecting numerical data.

## Numerical Data

Pandas will usually do a good job of detecting continuous numerical values if the only dirty data is empty strings or the strings "null", 'None', "NaN" or similar things. 

However, if your numbers are prices and contain dollar signs, or commas then pandas may just return them as strings.

So your continuous numerical value detector should be built around a function that detects numerical values given a single string or object.
It should return True or False.

The simplest possible implementation would be:

```python
def is_number(obj):
    try:
        x = float(obj)
        return True
    except ValueError:
        return False
```

You could then `.apply` this to a `DataFrame` column (`pd.Series`) like this:

```python
df['column_name'].apply(is_float)
```

Then you could count up the obvious `float` values in your in your column with the sum() method.
And you could use the percentage to decide whether a column is numerical or not:

```python
def is_numerical(series: pd.Series, min_purity=1.0):
    mask = series.apply(is_number)
    min_purity = min_purity if isinstance(min_purity, float) else min_purity / len(mask)
    return mask.sum() / len(mask) >= min_purity
```

## Categorical Data

Fortunately Pandas has a ["categorical" `dtype`](https://pandas.pydata.org/pandas-docs/stable/user_guide/categorical.html).
So your `is_categorical()` can "mark" those columns or Series that contain categorical data before the other `is_*` functions are run.
Here's how to change the dtype of a Pandas `Series` at the end of your `is_categorical` function.

```python
def is_categorical(series: pd.Series, max_cardinality=.1, lower=str.lower, strip=str.strip, replace=None):
    # coerce all objects in the series to strings (spoiler: `apply(str)`)
    # count the unique values which is the cardinality (spoiler: `.nunique()`)
    # compute the percentage of data that is unique (spoiler: num_unique / len(series))
    # once you know whether it's categorical or not, you can return False if it's not categorical, but if it is categorical, you can return the column itself, converted to a categorical column (a data type special to pandas dataframes) 
    return series.astype('categorical')
```

## Tag Data

If you find a list of tags in a column, that can be much harder to deal with.
Fortunately Martha Gavidia can give you a head start.
She created a function to process lists of genre tags for books in her internship project:

```python
def is_array(obj, sep=','):
    if isinstance(obj, str):
        try:
            return len(obj.split(sep)) > 1
        except ValueError:
            pass
    elif isinstance(obj, (list, tuple, np.ndarray, pd.Series)):
        return True
```

You need a function to convert an individual object into an array.

```python
def to_array(obj, array_type=set, sep=','):
    if isinstance(obj, str) and sep:
        # this is essentially a tokenizer
        try:
            return array_type(obj.split(sep))
        except ValueError:
            return array_type([obj])
    try:
        return array_type(obj)
    except ValueError:
        return array_type()
```

```python
def is_tags(series: pd.Series, max_cardinality=.1, lower=str.lower, strip=str.strip, replace=None):
    # use is_array() to find out if it's already an array of multiple values
    # use to_array() to coerce it into an array and make sure a few have more than one element
    s))
    # your can return the object itselfreturn series.astype('categorical')
```
    
## Natural Language

Your natural language detector is a function that takes as an input a dataframe and can then identify those columns that likely contain natural language.
You want it to discriminate natural language strings, such as descriptions or notes, from strings that contain a single date, category, or even a number.
So you can use all your other type detector functions, such as the is_numerical function above, to eliminate those other kinds of data science data.

Your is_categorical function probably checked the count of unique values (cardinality) for the column.
So that and your numerical object detector are probably good ones to run before this one.
Your function can also check to see that it is not a valid datetime, or street address/location data type.


## Goal

Your ultimate goal is to wrap all these function is a parent function that uses them all.
Your automatic datatype detector function will iterate through all the columns of a dataframe (provided as the only input argument).
It will run each of these data type detectors on each column and gradually build up lists of each type in a dictionary like this:

```python
def detect_dataframe_column_types(df):
    datatype_column_names = {
        'numerical': [],
        'categorical': [],
        'tags': [],
        'natural language': [],
        'unknown': []
    }
    for c in df.columns:
        # if `df[c]` is a numerical column then:
        #     append the column name to the list of numerical columns like this
        datatype_column_names['numerical'].append(c)
        # if `df[c]` is a categorical then:
        #     append the column name to the list of categorical columns like this
        datatype_column_names['categorical'].append(c)
        # check the other 2 data types before adding it to the 'unknown' data type:
        # ...
        # ...
        datatype_column_names['unknown'].append(c)
    return datatype_column_names    
```

Whomever has the best implementation will be invited to submit a mere request to the [proai package](https://gitlab.com/prosocialai/proai).
I will help improve and modularize your code during the merge request so that others can rely on it for their automatic ML pipelines.
