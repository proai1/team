# Your First Python Package

## Resources

- https://conf.sciwork.dev/2020/tutorial/packaging.html
- [Python Packaging User Guide](https://packaging.python.org/)
- [Conda-build documentation](https://docs.conda.io/projects/conda-build/)

## pypi.org

## setup.py

## setup.cfg

## twine

## anaconda

## [miniconda](https://conda.io/en/latest/miniconda.html)

## anaconda.org

- conda install
- `-c conda-forge`
- [`conda-build`]()
