# 3 - Data Engineering: Nutrition for Everyone

## FDA Nutrition Data Wrangling

- play with the [food data central (FDC) api](https://fdc.nal.usda.gov/api-spec/fdc_api.html)
- skim the [food data central (FDC) api docs](https://fdc.nal.usda.gov/api-guide.html)
- get an API key/token for FDC
- test your key using this [FDC api client](https://gist.github.com/noahtren/0305a32d2750ab4f5ce35ce0d191d139)

- images high res of labels
- 5000 images labeled with OUD and OUP
- data labeling
- Clarify
- automl (Google)
- OU, OUD, OUP
- OU and OUD logos for kosher certification

- personalization is not market segmentation
- they are going to fail, to not have a product after year
- patience is a 


# Possible milestones

- deploy public labelstudio instance
- deploy public object detector instance
- deploy public image classifier instance
- deploy public image segmenter
- transfer-learn an image classifier

- quality check training set
- three possible MVPs: enumerates the logos it sees (tags)
- object detector
- image segmenter 
