Do you think you can build an NLP model to detect misinformation in the news that you read?
It turns out, you can solve this problem with just a few basic features like the length of a title, or the capitalization of words in the title and the article body.

Here's a basic version of the problem that uses a dataset with a lot of leakage: [Fake News, Real NLP](https://gitlab.com/tangibleai/team/-/tree/master/learning-resources/projects/your-first-nlp-project--detect-fake-news.md)

If you'd like to preview some other upcoming exercises, you can find them in [team/learning-resources/tanbot-messages](https://gitlab.com/tangibleai/team/-/tree/master/learning-resources/tanbot-messages).
