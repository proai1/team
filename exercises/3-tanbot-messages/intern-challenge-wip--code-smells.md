# intern-challenge-12 Code Smells

This week you'll learn how to sniff out smelly code.

## Resources

- [Code review code smells](https://moderatemisbehaviour.github.io/clean-code-smells-and-heuristics/)
- [Beyond Basic Python](https://inventwithpython.com/beyond/) adapts the ideas to Python
