# Answer key

## Data generation

```python
>>> import numpy as np
>>> np.random.seed(1201780)
>>> x = np.round(5 * np.random.randn(200_000) + 6, 2)
>>> x = np.round(np.append(x, 3 + np.random.randn(800_000) - 4), 2)
>>> pd.Series(x).to_csv('x.csv', index=False)
```

## Solution

```python
>>> import pandas as pd
>>> x = pd.read_csv('x.csv')['0'].values
>>> np.mean(x)
0.4022049900000003
>>> np.mean(x[:500_000])
1.8027188800000011
>>> np.mean(x[:-250_000])
0.8689456666666672
>>> np.mean(x[:125_000])
6.01566264
>>> np.mean(x[:75_000])
6.005871333333334
>>> np.mean(x[:37_500])
6.001203200000001
>>> np.mean(x[:18_250])
5.971927671232877
>>> np.mean(x[:9_125])
5.983208767123288
>>> np.mean(x[:4_562])
5.937709338009644
>>> np.mean(x[:2_281])
5.942858395440596
```

## Bonus

